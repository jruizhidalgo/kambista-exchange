"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calcSueldo = exports.dniFormatValidation = void 0;

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//const { default: fetch } = require('node-fetch')
const dniValidation = dni => parseInt(dni.toString().substr(-1)) % 2 === 0 ? true : false;

const calcular = async sueldo => {
  const exec = await (0, _nodeFetch.default)(`https://api.kambista.com/v1/exchange/calculates?originCurrency=PEN&destinationCurrency=USD&active=S&amount=${sueldo}`);
  return exec.ok ? await exec.json() : 0;
};

const dniFormatValidation = params => {
  const dni = params.dni;
  return !isNaN(dni) && dni.length <= 8 ? dniValidation(dni) : 'Formato inválido de DNI. 8 caracteres';
};

exports.dniFormatValidation = dniFormatValidation;

const calcSueldo = async params => {
  let response;

  if (params.dni && params.sueldo) {
    const {
      dni,
      sueldo
    } = params;

    if (dni.toString().replace('.', '').length === 8) {
      if (dniValidation(dni)) {
        if (!isNaN(sueldo)) {
          const arrSueldo = sueldo.toString().split('.');
          const moneda = parseFloat(sueldo).toFixed(2);

          if (arrSueldo[0].length <= 6) {
            const kambista = await calcular(moneda);
            response = kambista ? {
              total: kambista.exchange
            } : 'Error intente de nuevo';
          } else {
            response = 'El valor del sueldo no debe exceder los 6 dígitos enteros';
          }
        } else {
          response = 'Formato inválido de sueldo';
        }
      } else {
        response = false;
      }
    } else {
      response = 'Formato inválido de DNI. 8 caracteres solo números';
    }
  } else {
    response = 'DNI y sueldo son necesarios';
  }

  return response;
}; //console.log(parseDecimalNumber("4872484.20"));


exports.calcSueldo = calcSueldo;