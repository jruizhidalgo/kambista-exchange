"use strict";

var _express = _interopRequireDefault(require("express"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _routes = _interopRequireDefault(require("./routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express.default)();
const PORT = process.env.PORT || 30;

_dotenv.default.config();

app.use(_express.default.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATH, DELETE, GET');
    return res.status(200).json({});
  }

  next();
}); // routes

app.get('/', (req, res) => res.sendFile(`${__dirname}/index.htm`));
app.use(_routes.default); // runing server

app.listen(PORT, () => console.log(`Server runnig on Port: ${PORT}`));
/*
npm i express node-fetch
npm i @babel/cli @babel/node @babel/core @babel/preset-env -D
*/