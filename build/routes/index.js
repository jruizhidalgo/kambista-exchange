"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _index = require("../controller/index");

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//const { dniFormatValidation, calcSueldo } = require('../controller/index.js')
//const route = require('express').Router()
const route = _express.default.Router();

const base = `/v1.0`;
route.get(`${base}/val-dni/`, (req, res) => res.send((0, _index.dniFormatValidation)(req.query)));
route.get(`${base}/calc-sueldo/`, async (req, res) => res.send(await (0, _index.calcSueldo)(req.query)));
route.post(`${base}/calc-sueldo/`, async (req, res) => res.send(await (0, _index.calcSueldo)(req.body)));
var _default = route;
exports.default = _default;