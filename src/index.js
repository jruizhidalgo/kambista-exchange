import express from 'express'
import dotenv from 'dotenv'
import route from './routes'
const app = express()
const PORT = process.env.PORT || 30
dotenv.config()

app.use(express.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATH, DELETE, GET')
        return res.status(200).json({})
    }
    next()
})

// routes
app.get('/', (req, res) => res.sendFile(`${__dirname}/index.htm`))
app.use(route)

// runing server
app.listen(PORT, () => console.log(`Server runnig on Port: ${PORT}`))



/*
npm i express node-fetch
npm i @babel/cli @babel/node @babel/core @babel/preset-env -D
*/
