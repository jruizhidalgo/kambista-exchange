//const { dniFormatValidation, calcSueldo } = require('../controller/index.js')
//const route = require('express').Router()
import { dniFormatValidation, calcSueldo } from '../controller/index'
import express from 'express'
const route = express.Router()
const base = `/v1.0`

route.get(`${base}/val-dni/`, (req, res) => res.send(dniFormatValidation(req.query)))
route.get(`${base}/calc-sueldo/`, async (req, res) => res.send(await calcSueldo(req.query)))
route.post(`${base}/calc-sueldo/`, async (req, res) => res.send(await calcSueldo(req.body)))

export default route